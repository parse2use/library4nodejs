const {
  default: ParseServer,
  ParseGraphQLServer, RedisCacheAdapter
} = require('parse-server');

const FtpHttpAdapter = require('parse2use');

/*********************************************************************************/

var fs = require('fs');

const { Servlet } = require("../found");

/*********************************************************************************/

exports.Wrapper = class extends Servlet {
    setup(cfg) {
        this.url = {
            parse: '/model/api',
            graph: '/model/gql',
            plays: '/model/iql',
        };

        this.mem = new RedisCacheAdapter({
            url: cfg.data.cache,
        });
        /*
        this.snd = {
            options: cfg.mail,
            module: "simple-parse-smtp-adapter",
        };

        this.vfs = new FtpHttpAdapter({
            ftp: {
                host:     "files.000webhost.com",               // required
                port:     21,                                   // defaults to 21
                username: "it-issal-shop",                      // defaults to "anonymous"
                password: "Testingham2020",                     // defaults to "anonymous@"
                path:     "/cdn/shopify",                       // defaults to "/"
            },
            http: {
                host: "http://it-issal-shop.000webhostapp.com", // required
                port: 80,                                       // defaults to 80
                path: "/cdn/memento",                           // defaults to "/"
            },
            debug: true                                         // defaults to false
        });
        //*/

        this.cfg = {
            appId: cfg.uuid,
            cloud: __dirname+"/cloud.js",

            serverURL: 'http://localhost:'+cfg.port+this.url['parse'],
            publicServerURL: cfg.link + this.url['parse'],

            maxUploadSize: cfg.size['upload']+'mb',

            masterKey:         cfg.keys.MASTER_K,
            readOnlyMasterKey: cfg.keys.TOKEN_RO,

            restAPIKey:        cfg.keys.REST_API,
            webhookKey:        cfg.keys.WEB_HOOK,

            clientKey:         cfg.keys.CLIENT_K,
            fileKey:           cfg.keys.FILE_KEY,

            javascriptKey:     cfg.keys.JAVASC_K,
            dotNetKey:         cfg.keys.DOTNET_K,

            auth: cfg.auth,
            push: {},

            databaseURI: cfg.data.sqldb,
            //databaseURI: process.env.DATALAKE_URL || cfg.data.nosql || cfg.data.sqldb,
            logsFolder: __dirname+'/../node_working/log/',

            // graph/sch/parse.graphql


        };

        if (cfg.data.alias!=null) {
            this.cfg.collectionPrefix = cfg.data.alias;
        };

        if (cfg.ldap!=null) {
            this.cfg['auth'].ldap = cfg.ldap;
        };

        if (this.mem!=null) {
            this.cfg.cacheAdapter = this.mem;
        }

        if (this.vfs!=null) {
            this.cfg.filesAdapter = this.vfs;
        }

        if (this.snd!=null) {
            this.cfg.emailAdapter = this.snd;
        }

        if (cfg.keys.ANDROIDS!=null) {
            this.cfg.push['android'] = {
                apiKey: cfg.keys.ANDROIDS,
            };
        }

        //if (cfg.keys.I_PHONES!=null) this.cfg.push['ios']  = cfg.keys.I_PHONES;
        //if (cfg.keys.TVOS_API!=null) this.cfg.push['tvos'] = cfg.keys.TVOS_API;

        this.api = new ParseServer(this.cfg);

        //*
        this.gql = new ParseGraphQLServer(this.api, {
            graphQLPath: this.url['graph'],
            playgroundPath: this.url['plays'],
        });
        //*/
    }

    /**************************************************************************/

    mount(app) {
        app.use(this.url['parse'], this.api.app); // (Optional) Mounts the REST API

        this.gql.applyGraphQL(app); // Mounts the GraphQL API
        this.gql.applyPlayground(app); // (Optional) Mounts the GraphQL Playground - do NOT use in Production

        //app.use("/model", web);
    }

    serve(srv) {
        ParseServer.createLiveQueryServer(srv);
    }

    /**************************************************************************/

    ready(url) {
        console.log('REST API running on : ' + url + this.url['parse']);
        console.log('GraphQL API running on : ' + url + this.url['graph']);
        console.log('GraphQL Playground running on : ' + url + this.url['plays']);
    }
};
