const fs = require('fs');
const path = require('path');

exports.serve = {};

const folder = path.resolve(__dirname,'serve');
const script = fs.readdirSync(folder);

for (i in script) {
    alias = script[i].replace('.js','');
    modus = require(folder+'/'+script[i]);
    /*
    try {
        modus = require(folder+'/'+script[i]);
    } catch (ex) {
        modus = null;

        raise(ex);
    }
    */
    if (modus) {
        exports.serve[alias] = modus;
    }
}
/*
const abstract = require('nodejs2use');

exports.Reactor = abstract.Reactor;
exports.Nucleon = abstract.Nucleon;

exports.Clients = abstract.Clients;
exports.Servlet = abstract.Servlet;
//*/
